Further information
===================

The core AOFlagger algorithms are described in the following papers:

* `An interference detection strategy for Apertif based on AOFlagger 3 <https://arxiv.org/abs/2301.01562>`_, Offringa, Adebahr, Kutkin et al. (2023), A&A, 670, A166.

* `A morphological algorithm for improving radio-frequency interference detection <https://arxiv.org/abs/1201.3364>`_, Offringa, Van de Gronde and Roerdink (2012), A&A, 539, 10, A95.

* `Post-correlation radio frequency interference classification methods <https://arxiv.org/abs/1002.1957>`_, Offringa et al. (2010), MNRAS 405, 155-167.

Flagging results for LOFAR and MWA using the AOFlagger are described in the following papers, respectively:

* `The LOFAR radio environment <https://arxiv.org/abs/1210.0393>`_, Offringa et al. (2013), A&A, 549, A11.

* `The low-frequency environment of the MWA: RFI analysis and mitigation <https://arxiv.org/abs/1501.03946>`_, Offringa et al. (2015), PASA, 32, e008.

Other related papers:

* `RFI flagging in solar and space weather low frequency radio observations <https://academic.oup.com/mnras/article/521/1/630/7036782>`_, Zhang, Offringa, Zucca et al. (2023), MNRAS, 512, 1, 630-637.
