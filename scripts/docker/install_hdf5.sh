# Script to install HDF5 from source

set -euo pipefail

echo "Installing zlib-devel with yum"
yum -y install zlib-devel

pushd /tmp

echo "Downloading & unpacking HDF5 ${HDF5_VERSION}"
curl -fsSLO "https://hdf-wordpress-1.s3.amazonaws.com/wp-content/uploads/manual/HDF5/HDF5_${HDF5_VERSION//./_}/source/hdf5-${HDF5_VERSION}.tar.gz"
tar -xzvf hdf5-${HDF5_VERSION}.tar.gz
pushd hdf5-${HDF5_VERSION}

echo "Configuring, building & installing HDF5 ${HDF5_VERSION} to ${HDF5_DIR}"
./configure --prefix ${HDF5_DIR} --enable-build-mode=production --enable-cxx
make -j $(nproc)
make install
popd

# Clean up to limit the size of the Docker image
echo "Cleaning up unnecessary files"
rm -r hdf5-${HDF5_VERSION}
rm hdf5-${HDF5_VERSION}.tar.gz

yum -y erase zlib-devel