#ifndef PLOT_STATISTICS_H_
#define PLOT_STATISTICS_H_

#include "imageinterface.h"

void WinsorizedMeanAndStdDev(const ImageInterface& image, float& mean,
                             float& stddev);

#endif
